# Smartlane OpenAPI documentation

Smartlane OpenAPI/Swagger documentation specified as a .yaml file, according to [OpenAPI-Specification 2.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md).
